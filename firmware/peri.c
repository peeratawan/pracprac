#include <avr/io.h>
#include "peri.h"

void init_peripheral()
{		
		DDRB |= (1 << PB1)|(1 << PB3);
		DDRC |= (1 << PC1)|(1 << PC3);
		DDRD |= (1 << PD6);
		PORTB &= ~((1 << PB5) |(1 << PB3));
		PORTC &= ~((1 << PC1) |(1 << PC3));
		PORTD &= ~(1 << PD6);

		DDRB &= ~((1 << PB0)|(1 << PB2));
		DDRC &= ~((1 << PC0)|(1 << PC2));
		DDRD &= ~(1 << PD5);
		PORTB |= (1 << PB0) |(1 << PB2);
		PORTC |= (1 << PC0) |(1 << PC2);
		PORTD |= (1 << PD5);


}

void set_led(uint8_t pin, uint8_t state)
{
    	if(state==0){
		 	PORTB &= ~(1<<pin);
		 	PORTC &= ~(1<<pin);
		 	PORTD &= ~(1<<pin);
		}
		else{
			PORTB |= (1<<pin);
			PORTC |= (1<<pin);
			PORTD |= (1<<pin);
		}
}

void set_led_value(uint8_t value)
{
    PORTB &= ~(0b00000111); 
	 PORTB |= (value & 0b00000111);
    PORTC &= ~(0b00000111); 
	 PORTC |= (value & 0b00000111);
    PORTD &= ~(0b00000111); 
	 PORTD |= (value & 0b00000111);
}
