#define IS_SWITCH_PRESSED1() !(PINB &(1<<PB0))
#define IS_SWITCH_PRESSED2() !(PINB &(1<<PB2))
#define IS_SWITCH_PRESSED3() !(PINC &(1<<PC2))
#define IS_SWITCH_PRESSED4() !(PIND &(1<<PD5))


void init_peripheral();
void set_led(uint8_t pin, uint8_t state);
void set_led_value(uint8_t value);
uint16_t read_adc(uint8_t channel);
uint16_t get_light();
